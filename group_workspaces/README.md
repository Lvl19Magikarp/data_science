Group workspaces is intended to store workspaces where multiple people work together on a single file. Still no pushing directly to master, branch, squash and merge/rebase. I advise looking at how pull requests work to ensure you don't overwrite each others work but I'm not the boss of you.

