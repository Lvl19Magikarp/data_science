Repo for the spring 2020 Data Science coursework

Please clone the workspace, then create a seperate branch before beginning to work. Merging to master should be done via a squash then rebase on the branch.

Squashing removes all the ugly commit messages with a single message of your choosing, rebasing prevents all sorts of ugly merge conflicts.

Dont edit any individual workspace beyond your own to keep any git silliness to a minimum.

Name branches [USERNAME]_[Whatever you like] so we always know whose branch is whose

Master should only go forward, rewinding history on public branches can cause nightmare fuel that makes it easier to burn the entire repo and start again.